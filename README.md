# API

Java Spring REST API gateway for manipulating Zeebe workflows instances and exposing workflow instance variables from elasticsearch. Serves as a back-end for the chat front-end.

## Requirements

 - docker
 - docker-compose
 - jdk 1.8
 - [Zeebe DMN Worker](https://github.com/zeebe-io/zeebe-dmn-worker) built in `../zeebe-dmn-worker/target/`
 - [Zeebe Docker Compose](https://github.com/zeebe-io/zeebe-docker-compose/blob/master/operate-simple-monitor/docker-compose.yml) up and running with the `operate-simple-monitor` configuration

## Getting started

### Runnning the operate simple monitor containers
```
$ cd ../zeebe-docker-compose/operate-simple-monitor
$ docker-compose up -d
```

### Running the DMN Service Worker
```
$ java -jar ../zeebe-dmn-worker/target/zeebe-dmn-worker.jar
```

### Running the REST API
```
$ mvn package
$ java -jar -Dserver.port=8888 target/gs-rest-service-0.1.0.jar
```
