package it.vanmuijden.vng.zeebe.controllers;

import java.time.Duration;
import java.util.HashMap;
import java.util.Map;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import io.zeebe.client.ZeebeClient;
import it.vanmuijden.vng.zeebe.clients.ZeebeClientSingleton;
import it.vanmuijden.vng.zeebe.dto.Address;

@RestController
public class AddressController {

    @PostMapping("/api/workflows/{workflowId}/address")
    public Address create(@PathVariable Long workflowId, @RequestBody Address address) {
	ZeebeClient client = ZeebeClientSingleton.getInstance();
	
	final Map<String, Object> data = new HashMap<>();
	data.put("housenumber", address.getHouseNumber());
	client.newPublishMessageCommand()
                .messageName("housenumber-provided")
                .correlationKey(String.valueOf(workflowId))
                .timeToLive(Duration.ofSeconds(30))
                .variables(data)
                .send()
                .join();
	
	return address;
    }
}