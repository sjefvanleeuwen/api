package it.vanmuijden.vng.zeebe.dto;

import java.util.Map;

public class WorkflowVariables {

    private final Boolean done;
    private final Boolean goedgekeurd;
    private final String resource;
    private final String field;
    private final String explanation;
    private final String attribute;
    
    public WorkflowVariables(Map<String, Object> variables) {
	this.done = Boolean.parseBoolean(String.valueOf(variables.getOrDefault("done", "")));
        this.goedgekeurd = Boolean.parseBoolean(String.valueOf(variables.getOrDefault("goedgekeurd", "")));
        this.resource = ((String) variables.getOrDefault("resource", "")).replaceAll("\"", "");
        this.field = ((String) variables.getOrDefault("field", "")).replaceAll("\"", "");
        this.explanation = ((String) variables.getOrDefault("explanation", "")).replaceAll("\"", "");
        this.attribute = ((String) variables.getOrDefault("attribute", "")).replaceAll("\"", "");
    }

    public Boolean getDone() {
	return done;
    }

    public Boolean getGoedgekeurd() {
	return goedgekeurd;
    }

    public String getResource() {
	return resource;
    }

    public String getField() {
	return field;
    }

    public String getExplanation() {
	return explanation;
    }

    public String getAttribute() {
        return attribute;
    }
}
