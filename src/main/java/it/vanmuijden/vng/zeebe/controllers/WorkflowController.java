package it.vanmuijden.vng.zeebe.controllers;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.bucket.filter.ParsedFilter;
import org.elasticsearch.search.aggregations.bucket.terms.IncludeExclude;
import org.elasticsearch.search.aggregations.bucket.terms.Terms;
import org.elasticsearch.search.aggregations.bucket.terms.Terms.Bucket;
import org.elasticsearch.search.aggregations.metrics.TopHits;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.sort.SortOrder;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import io.zeebe.client.ZeebeClient;
import io.zeebe.client.api.events.WorkflowInstanceEvent;
import it.vanmuijden.vng.zeebe.clients.RestHighLevelClientSingleton;
import it.vanmuijden.vng.zeebe.clients.ZeebeClientSingleton;
import it.vanmuijden.vng.zeebe.dto.Workflow;
import it.vanmuijden.vng.zeebe.dto.WorkflowVariables;

@RestController
public class WorkflowController {
    
    @PostMapping("/api/workflows")
    public Workflow create(@RequestBody(required=false) Workflow workflow) {
	ZeebeClient client = ZeebeClientSingleton.getInstance();
	final WorkflowInstanceEvent workflowInstance = client.newCreateInstanceCommand()
                .bpmnProcessId("regelingen")
                .latestVersion()
                .send()
                .join();

	final long workflowInstanceKey = workflowInstance.getWorkflowInstanceKey();
	
	final Map<String, Object> data = new HashMap<>();
	data.put("workflowId", workflowInstanceKey);
	client.newSetVariablesCommand(workflowInstanceKey)
		.variables(data)
		.send()
		.join();
	
        return new Workflow(workflowInstanceKey);
    }
    
    @GetMapping("/api/workflows/{workflowId}")
    public Workflow get(@PathVariable Long workflowId) {
	
	try {
	    RestHighLevelClient client = RestHighLevelClientSingleton.getInstance();
	    SearchRequest searchRequest = new SearchRequest("zeebe-record-workflow-instance_*")
		    .source(new SearchSourceBuilder()
    		            .query(QueryBuilders.termQuery("value.workflowInstanceKey", workflowId))
    		            .from(0)
        		    .size(1)
        		    .sort("timestamp", SortOrder.DESC));
	    
	    SearchResponse searchResponse = client.search(searchRequest, RequestOptions.DEFAULT);
	    
	    SearchHits searchHits = searchResponse.getHits();
	    if (searchHits.getTotalHits().value == 0) {
		throw new ResourceNotFoundException();
	    }
	    
	    SearchHit searchHit = searchHits.getAt(0);
	    Map<String, Object> source = searchHit.getSourceAsMap();
	    
	    String elementType = "";
	    String intent = "";
	    
	    if (source.containsKey("value")) {
		Object valueObject = source.get("value");
		System.out.println("It contains a value");
		if (valueObject instanceof Map<?, ?>) {
		    System.out.println("It is the right type");
		    @SuppressWarnings("unchecked")
		    Map<String, Object> value = (Map<String, Object>)valueObject;
		    elementType = String.valueOf(value.get("bpmnElementType"));
		}  
	    }
	    
	    if (source.containsKey("metadata")) {
		Object metadataObject = source.get("metadata");
		System.out.println("It contains a value");
		if (metadataObject instanceof Map<?, ?>) {
		    System.out.println("It is the right type");
		    @SuppressWarnings("unchecked")
		    Map<String, Object> metadata = (Map<String, Object>)metadataObject;
		    intent = String.valueOf(metadata.get("intent"));
		}
	    }
	    
	    Workflow.State state;
	    
	    if (intent.equals("ELEMENT_COMPLETED") && elementType.equals("PROCESS")) {
		state = Workflow.State.COMPLETE;
	    } else if (intent.equals("ELEMENT_ACTIVATED") && elementType.equals("INTERMEDIATE_CATCH_EVENT")) {
		state = Workflow.State.READY;
	    } else {
		state = Workflow.State.BUSY;
	    }
	    
	    
	    searchRequest = new SearchRequest("zeebe-record-variable_*");
	    searchRequest.source(new SearchSourceBuilder()
		    .size(0)
		    .aggregation(AggregationBuilders.filter("variables", QueryBuilders.termQuery("value.workflowInstanceKey", workflowId))
			    .subAggregation(
				    AggregationBuilders.terms("names")
				    .field("value.name")
				    .includeExclude(new IncludeExclude(new String[] { "done", "goedgekeurd", "resource", "field", "explanation", "attribute" }, null))
				    .subAggregation(
					    AggregationBuilders.topHits("last_value")
					    .size(1)
					    .sort("timestamp", SortOrder.DESC)
					    .fetchSource(new String[] {"value.value"}, null)))));
	    
	    searchResponse = client.search(searchRequest, RequestOptions.DEFAULT);
	    
	    Map<String, Object> data = new HashMap<>();
	    ParsedFilter parsedFilter = searchResponse.getAggregations().get("variables");
	    Terms variables = parsedFilter.getAggregations().get("names");
	    
	    for (Bucket entry : variables.getBuckets()) {
		    String key = entry.getKeyAsString();
		    TopHits topHits = entry.getAggregations().get("last_value");
		    for (SearchHit hit : topHits.getHits().getHits()) {
			System.out.printf(" -> id [%s], _source [%s]", String.valueOf(hit.getId()), hit.getSourceAsMap().get("value").toString());
			Map<String, Object> value = (Map<String, Object>) hit.getSourceAsMap().get("value");
			data.put(key, value.get("value"));
		    }
	    }
	    
	    if (data.isEmpty()) {	
		return new Workflow(workflowId, state);
	    }
	    
	    WorkflowVariables workflowVariables = new WorkflowVariables(data);
	    
	    return new Workflow(workflowId, state, workflowVariables);
	    
	} catch (IOException e) {
	    e.printStackTrace();
	}
	
	return new Workflow(workflowId);
    }
    
    @ResponseStatus(value = HttpStatus.NOT_FOUND)
    public class ResourceNotFoundException extends RuntimeException {
	private static final long serialVersionUID = 1L;
    }
}
