package it.vanmuijden.vng.zeebe.dto;

public class Workflow {

    public enum State {
	UNKNOWN,
	BUSY,
	READY,
	COMPLETE
    }
    private final long id;
    private final WorkflowVariables variables;
    private final State state;


    public Workflow(long id) {
        this.id = id;
        this.variables = null;
        this.state = State.UNKNOWN;
    }
    public Workflow(long id, State state) {
        this.id = id;
        this.variables = null;
        this.state = state;
    }
    
    public Workflow(long id, State state, WorkflowVariables variables) {
	this.id = id;
	this.variables = variables;
	this.state = state;
    }

    public long getId() {
        return id;
    }
    
    public State getState() {
        return state;
    }
    
    public WorkflowVariables getVariables() {
	return variables;
    }
}
