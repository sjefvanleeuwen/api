package it.vanmuijden.vng.zeebe.clients;

import javax.annotation.PreDestroy;

import org.springframework.stereotype.Component;

import io.zeebe.client.ZeebeClient;

@Component
public class ZeebeClientSingleton {
	
	private static ZeebeClient instance = null;
	
	protected ZeebeClientSingleton() {}
	
	public static ZeebeClient getInstance() {
	    if(instance == null) {
	        instance = ZeebeClient.newClientBuilder()
	        	.brokerContactPoint("127.0.0.1:26500")
	        	.build();
	    }
	    return instance;
	}
	
	@PreDestroy
	public void preDestroy() {
	    if(instance == null) {
		return;
	    }
	    instance.close();
	}
}
