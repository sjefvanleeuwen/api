package it.vanmuijden.vng.zeebe.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Address {

    private int houseNumber;
    
    @JsonCreator
    public Address(@JsonProperty("houseNumber") int houseNumber) {
	this.houseNumber = houseNumber;
    }
    
    public int getHouseNumber() {
	return this.houseNumber;
    }
    
    public void setHouseNumber(int houseNumber) {
	this.houseNumber = houseNumber;
    }
}
