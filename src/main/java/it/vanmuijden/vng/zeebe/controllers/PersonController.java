package it.vanmuijden.vng.zeebe.controllers;

import java.time.Duration;
import java.util.HashMap;
import java.util.Map;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import io.zeebe.client.ZeebeClient;
import it.vanmuijden.vng.zeebe.clients.ZeebeClientSingleton;
import it.vanmuijden.vng.zeebe.dto.Person;

@RestController
public class PersonController {

    @PostMapping("/api/workflows/{workflowId}/person")
    public Person create(@PathVariable Long workflowId, @RequestBody Person person) {
	ZeebeClient client = ZeebeClientSingleton.getInstance();
	
	final Map<String, Object> data = new HashMap<>();
	data.put("adult", person.getAdult());
	client.newPublishMessageCommand()
                .messageName("adult-provided")
                .correlationKey(String.valueOf(workflowId))
                .timeToLive(Duration.ofSeconds(30))
                .variables(data)
                .send()
                .join();
	
	return person;
    }
}