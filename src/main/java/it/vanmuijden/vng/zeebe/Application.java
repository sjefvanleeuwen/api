package it.vanmuijden.vng.zeebe;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import it.vanmuijden.vng.zeebe.clients.ZeebeClientSingleton;

@SpringBootApplication
public class Application {

    public static void main(String[] args) {
	ZeebeClientSingleton.getInstance().newDeployCommand()
	            .addResourceFromClasspath("regelingen.bpmn")
	            .send()
	            .join();
	        
        SpringApplication.run(Application.class, args);
    }
}
