package it.vanmuijden.vng.zeebe.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Person {

    private boolean adult;
    
    @JsonCreator
    public Person(@JsonProperty("adult") String adult) {
        this.adult = Boolean.valueOf(adult);
    }

    public boolean getAdult() {
	return this.adult;
    }
    
    public void setAdult(String adult) {
	this.adult = adult.equals("yes");
    }
}
