package it.vanmuijden.vng.zeebe.clients;

import java.io.IOException;

import javax.annotation.PreDestroy;

import org.apache.http.HttpHost;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.springframework.stereotype.Component;

@Component
public class RestHighLevelClientSingleton {
	
    private static final String ELASTICSEARCH_PROTOCOL = "http";
    private static final String ELASTICSEARCH_HOST = "localhost";
    private static final int ELASTICSEARCH_PORT = 9200;
    
    private static RestHighLevelClient instance = null;
	
    protected RestHighLevelClientSingleton() {}
	
    public static RestHighLevelClient getInstance() {
	if(instance == null) {
	    instance = new RestHighLevelClient(
		    RestClient.builder(
			    new HttpHost(ELASTICSEARCH_HOST, ELASTICSEARCH_PORT, ELASTICSEARCH_PROTOCOL)));
	}
	    
	return instance;
    }
	
    @PreDestroy
    public void preDestroy() {
	if(instance == null) {
	    return;
	}
	try {
	    instance.close();
	} catch (IOException e) {
	    e.printStackTrace();
	}
    }
}
